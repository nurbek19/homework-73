const express = require('express');
const app = express();
const port = 8000;

app.get('/:request', (req, res) => {
    res.send(req.params.request);
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});
