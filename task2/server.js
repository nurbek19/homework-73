const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

const password = 'jaskdasjdk';

app.get('/', (req, res) => {
    res.send('Hello world!!!');
});

app.get('/encode/:text', (req, res) => {
    const response = Vigenere.Cipher(password).crypt(req.params.text);
    res.send(response);
});

app.get('/decode/:text', (req, res) => {
    const response = Vigenere.Decipher(password).crypt(req.params.text);
    res.send(response);
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});